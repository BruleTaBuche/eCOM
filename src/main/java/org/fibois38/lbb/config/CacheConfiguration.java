package org.fibois38.lbb.config;

import java.time.Duration;

import org.ehcache.config.builders.*;
import org.ehcache.jsr107.Eh107Configuration;

import io.github.jhipster.config.jcache.BeanClassLoaderAwareJCacheRegionFactory;
import io.github.jhipster.config.JHipsterProperties;

import org.springframework.boot.autoconfigure.cache.JCacheManagerCustomizer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.*;

@Configuration
@EnableCaching
public class CacheConfiguration {

    private final javax.cache.configuration.Configuration<Object, Object> jcacheConfiguration;

    public CacheConfiguration(JHipsterProperties jHipsterProperties) {
        BeanClassLoaderAwareJCacheRegionFactory.setBeanClassLoader(this.getClass().getClassLoader());
        JHipsterProperties.Cache.Ehcache ehcache =
            jHipsterProperties.getCache().getEhcache();

        jcacheConfiguration = Eh107Configuration.fromEhcacheCacheConfiguration(
            CacheConfigurationBuilder.newCacheConfigurationBuilder(Object.class, Object.class,
                ResourcePoolsBuilder.heap(ehcache.getMaxEntries()))
                .withExpiry(ExpiryPolicyBuilder.timeToLiveExpiration(Duration.ofSeconds(ehcache.getTimeToLiveSeconds())))
                .build());
    }

    @Bean
    public JCacheManagerCustomizer cacheManagerCustomizer() {
        return cm -> {
            cm.createCache(org.fibois38.lbb.repository.UserRepository.USERS_BY_LOGIN_CACHE, jcacheConfiguration);
            cm.createCache(org.fibois38.lbb.repository.UserRepository.USERS_BY_EMAIL_CACHE, jcacheConfiguration);
            cm.createCache(org.fibois38.lbb.domain.User.class.getName(), jcacheConfiguration);
            cm.createCache(org.fibois38.lbb.domain.Authority.class.getName(), jcacheConfiguration);
            cm.createCache(org.fibois38.lbb.domain.User.class.getName() + ".authorities", jcacheConfiguration);
            cm.createCache(org.fibois38.lbb.domain.Bois.class.getName(), jcacheConfiguration);
            cm.createCache(org.fibois38.lbb.domain.Bois.class.getName() + ".offres", jcacheConfiguration);
            cm.createCache(org.fibois38.lbb.domain.Client.class.getName(), jcacheConfiguration);
            cm.createCache(org.fibois38.lbb.domain.Client.class.getName() + ".communes", jcacheConfiguration);
            cm.createCache(org.fibois38.lbb.domain.Client.class.getName() + ".commandes", jcacheConfiguration);
            cm.createCache(org.fibois38.lbb.domain.Commande.class.getName(), jcacheConfiguration);
            cm.createCache(org.fibois38.lbb.domain.Producteur.class.getName(), jcacheConfiguration);
            cm.createCache(org.fibois38.lbb.domain.Producteur.class.getName() + ".offres", jcacheConfiguration);
            cm.createCache(org.fibois38.lbb.domain.Producteur.class.getName() + ".communes", jcacheConfiguration);
            cm.createCache(org.fibois38.lbb.domain.Offre.class.getName(), jcacheConfiguration);
            cm.createCache(org.fibois38.lbb.domain.Offre.class.getName() + ".communes", jcacheConfiguration);
            cm.createCache(org.fibois38.lbb.domain.Offre.class.getName() + ".commandes", jcacheConfiguration);
            cm.createCache(org.fibois38.lbb.domain.Commune.class.getName(), jcacheConfiguration);
            cm.createCache(org.fibois38.lbb.domain.Commune.class.getName() + ".producteurs", jcacheConfiguration);
            cm.createCache(org.fibois38.lbb.domain.Commune.class.getName() + ".offres", jcacheConfiguration);
            cm.createCache(org.fibois38.lbb.domain.Commune.class.getName() + ".clients", jcacheConfiguration);
            // jhipster-needle-ehcache-add-entry
        };
    }
}
