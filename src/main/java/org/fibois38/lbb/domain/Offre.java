package org.fibois38.lbb.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import org.fibois38.lbb.domain.enumeration.Frequence;

/**
 * A Offre.
 */
@Entity
@Table(name = "offre")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Offre implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "nom", nullable = false)
    private String nom;

    @NotNull
    @DecimalMin(value = "0")
    @Column(name = "prix", nullable = false)
    private Double prix;

    @NotNull
    @Column(name = "taille", nullable = false)
    private Integer taille;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "frequence", nullable = false)
    private Frequence frequence;

    @Lob
    @Column(name = "description")
    private String description;

    @NotNull
    @Min(value = 0)
    @Column(name = "stock", nullable = false)
    private Integer stock;

    @NotNull
    @Column(name = "date_livraison", nullable = false)
    private LocalDate dateLivraison;

    @NotNull
    @Column(name = "date_publication", nullable = false)
    private LocalDate datePublication;

    @ManyToOne
    //@JsonIgnoreProperties("offres")
    private Bois bois;

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "offre_commune",
               joinColumns = @JoinColumn(name = "offres_id", referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "communes_id", referencedColumnName = "id"))
    private Set<Commune> communes = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties("offres")
    private Producteur producteur;

    @OneToMany(mappedBy = "offre")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Commande> commandes = new HashSet<>();
    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public Offre nom(String nom) {
        this.nom = nom;
        return this;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Double getPrix() {
        return prix;
    }

    public Offre prix(Double prix) {
        this.prix = prix;
        return this;
    }

    public void setPrix(Double prix) {
        this.prix = prix;
    }

    public Integer getTaille() {
        return taille;
    }

    public Offre taille(Integer taille) {
        this.taille = taille;
        return this;
    }

    public void setTaille(Integer taille) {
        this.taille = taille;
    }

    public Frequence getFrequence() {
        return frequence;
    }

    public Offre frequence(Frequence frequence) {
        this.frequence = frequence;
        return this;
    }

    public void setFrequence(Frequence frequence) {
        this.frequence = frequence;
    }

    public String getDescription() {
        return description;
    }

    public Offre description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getStock() {
        return stock;
    }

    public Offre stock(Integer stock) {
        this.stock = stock;
        return this;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    public LocalDate getDateLivraison() {
        return dateLivraison;
    }

    public Offre dateLivraison(LocalDate dateLivraison) {
        this.dateLivraison = dateLivraison;
        return this;
    }

    public void setDateLivraison(LocalDate dateLivraison) {
        this.dateLivraison = dateLivraison;
    }

    public LocalDate getDatePublication() {
        return datePublication;
    }

    public Offre datePublication(LocalDate datePublication) {
        this.datePublication = datePublication;
        return this;
    }

    public void setDatePublication(LocalDate datePublication) {
        this.datePublication = datePublication;
    }

    public Bois getBois() {
        return bois;
    }

    public Offre bois(Bois bois) {
        this.bois = bois;
        return this;
    }

    public void setBois(Bois bois) {
        this.bois = bois;
    }

    public Set<Commune> getCommunes() {
        return communes;
    }

    public Offre communes(Set<Commune> communes) {
        this.communes = communes;
        return this;
    }

    public Offre addCommune(Commune commune) {
        this.communes.add(commune);
        commune.getOffres().add(this);
        return this;
    }

    public Offre removeCommune(Commune commune) {
        this.communes.remove(commune);
        commune.getOffres().remove(this);
        return this;
    }

    public void setCommunes(Set<Commune> communes) {
        this.communes = communes;
    }

    public Producteur getProducteur() {
        return producteur;
    }

    public Offre producteur(Producteur producteur) {
        this.producteur = producteur;
        return this;
    }

    public void setProducteur(Producteur producteur) {
        this.producteur = producteur;
    }

    public Set<Commande> getCommandes() {
        return commandes;
    }

    public Offre commandes(Set<Commande> commandes) {
        this.commandes = commandes;
        return this;
    }

    public Offre addCommande(Commande commande) {
        this.commandes.add(commande);
        commande.setOffre(this);
        return this;
    }

    public Offre removeCommande(Commande commande) {
        this.commandes.remove(commande);
        commande.setOffre(null);
        return this;
    }

    public void setCommandes(Set<Commande> commandes) {
        this.commandes = commandes;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Offre offre = (Offre) o;
        if (offre.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), offre.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Offre{" +
            "id=" + getId() +
            ", nom='" + getNom() + "'" +
            ", prix=" + getPrix() +
            ", taille=" + getTaille() +
            ", frequence='" + getFrequence() + "'" +
            ", description='" + getDescription() + "'" +
            ", stock=" + getStock() +
            ", dateLivraison='" + getDateLivraison() + "'" +
            ", datePublication='" + getDatePublication() + "'" +
            "}";
    }
}
