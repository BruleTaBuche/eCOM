package org.fibois38.lbb.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Producteur.
 */
@Entity
@Table(name = "producteur")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Producteur implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "email", nullable = false)
    private String email;

    @Lob
    @Column(name = "photo")
    private byte[] photo;

    @Column(name = "photo_content_type")
    private String photoContentType;

    @NotNull
    @Column(name = "nom", nullable = false)
    private String nom;

    @NotNull
    @Column(name = "prenom", nullable = false)
    private String prenom;

    
    @Lob
    @Column(name = "jhi_password", nullable = false)
    private String password;

    @Lob
    @Column(name = "description")
    private String description;

    @OneToMany(mappedBy = "producteur")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Offre> offres = new HashSet<>();
    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "producteur_commune",
               joinColumns = @JoinColumn(name = "producteurs_id", referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "communes_id", referencedColumnName = "id"))
    private Set<Commune> communes = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public Producteur email(String email) {
        this.email = email;
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public byte[] getPhoto() {
        return photo;
    }

    public Producteur photo(byte[] photo) {
        this.photo = photo;
        return this;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

    public String getPhotoContentType() {
        return photoContentType;
    }

    public Producteur photoContentType(String photoContentType) {
        this.photoContentType = photoContentType;
        return this;
    }

    public void setPhotoContentType(String photoContentType) {
        this.photoContentType = photoContentType;
    }

    public String getNom() {
        return nom;
    }

    public Producteur nom(String nom) {
        this.nom = nom;
        return this;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public Producteur prenom(String prenom) {
        this.prenom = prenom;
        return this;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getPassword() {
        return password;
    }

    public Producteur password(String password) {
        this.password = password;
        return this;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDescription() {
        return description;
    }

    public Producteur description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<Offre> getOffres() {
        return offres;
    }

    public Producteur offres(Set<Offre> offres) {
        this.offres = offres;
        return this;
    }

    public Producteur addOffre(Offre offre) {
        this.offres.add(offre);
        offre.setProducteur(this);
        return this;
    }

    public Producteur removeOffre(Offre offre) {
        this.offres.remove(offre);
        offre.setProducteur(null);
        return this;
    }

    public void setOffres(Set<Offre> offres) {
        this.offres = offres;
    }

    public Set<Commune> getCommunes() {
        return communes;
    }

    public Producteur communes(Set<Commune> communes) {
        this.communes = communes;
        return this;
    }

    public Producteur addCommune(Commune commune) {
        this.communes.add(commune);
        commune.getProducteurs().add(this);
        return this;
    }

    public Producteur removeCommune(Commune commune) {
        this.communes.remove(commune);
        commune.getProducteurs().remove(this);
        return this;
    }

    public void setCommunes(Set<Commune> communes) {
        this.communes = communes;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Producteur producteur = (Producteur) o;
        if (producteur.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), producteur.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Producteur{" +
            "id=" + getId() +
            ", email='" + getEmail() + "'" +
            ", photo='" + getPhoto() + "'" +
            ", photoContentType='" + getPhotoContentType() + "'" +
            ", nom='" + getNom() + "'" +
            ", prenom='" + getPrenom() + "'" +
            ", password='" + getPassword() + "'" +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
