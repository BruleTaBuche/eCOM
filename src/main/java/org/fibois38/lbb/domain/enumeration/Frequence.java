package org.fibois38.lbb.domain.enumeration;

/**
 * The Frequence enumeration.
 */
public enum Frequence {
    PONCTUEL, QUOTIDIEN, HEBDOMADAIRE, MENSUEL
}
