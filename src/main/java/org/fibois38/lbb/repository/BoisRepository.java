package org.fibois38.lbb.repository;

import java.util.List;

import org.fibois38.lbb.domain.Bois;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data repository for the Bois entity.
 */
@SuppressWarnings("unused")
@Repository
public interface BoisRepository extends JpaRepository<Bois, Long> {
	@Query(value = "select distinct bois.essence from Bois bois")
	List<String> findDistinctByEssence();
}
