package org.fibois38.lbb.repository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.fibois38.lbb.domain.Offre;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data repository for the Offre entity.
 */
@SuppressWarnings("unused")
@Repository
public interface OffreRepository extends JpaRepository<Offre, Long> {

	@Query(value = "select distinct offre from Offre offre left join fetch offre.communes", countQuery = "select count(distinct offre) from Offre offre")
	Page<Offre> findAllWithEagerRelationships(Pageable pageable);

	@Query(value = "select distinct offre from Offre offre left join fetch offre.communes")
	List<Offre> findAllWithEagerRelationships();

	@Query("select offre from Offre offre left join fetch offre.communes where offre.id =:id")
	Optional<Offre> findOneWithEagerRelationships(@Param("id") Long id);

	@Query(value = "select distinct offre from Offre offre where offre.taille in :taille")
	Page<Offre> findByTailleIn(Pageable pageable, @Param("taille") Collection<Integer> tailleList);

	@Query(value = "select distinct offre from Offre offre where offre.taille in :taille and offre.bois.essence in :essences")
	Page<Offre> findByTailleInAndBoisIn(Pageable pageable, @Param("taille") Collection<Integer> tailleList,
			@Param("essences") Collection<String> essenceList);

	@Query(value = "select distinct offre from Offre offre left join offre.communes commune where offre.taille in :taille and offre.bois.essence in :essences and commune.id=:communeid")
	Page<Offre> findByTailleInAndBoisInAndCommune(Pageable pageable, @Param("taille") Collection<Integer> tailleList,
			@Param("essences") Collection<String> essenceList, @Param("communeid") Long commune);

	@Query(value = "select distinct offre.taille from Offre offre")
	List<Integer> findDistinctByTaille();
}
