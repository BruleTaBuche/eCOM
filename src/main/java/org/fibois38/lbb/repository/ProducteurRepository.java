package org.fibois38.lbb.repository;

import org.fibois38.lbb.domain.Producteur;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

/**
 * Spring Data repository for the Producteur entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ProducteurRepository extends JpaRepository<Producteur, Long> {

	@Query(value = "select distinct producteur from Producteur producteur left join fetch producteur.communes", countQuery = "select count(distinct producteur) from Producteur producteur")
	Page<Producteur> findAllWithEagerRelationships(Pageable pageable);

	@Query(value = "select distinct producteur from Producteur producteur left join fetch producteur.communes")
	List<Producteur> findAllWithEagerRelationships();

	@Query("select producteur from Producteur producteur left join fetch producteur.communes where producteur.id =:id")
	Optional<Producteur> findOneWithEagerRelationships(@Param("id") Long id);

	@Query(value = "select distinct producteur from Producteur producteur left join producteur.offres offre left join offre.communes commune where offre.taille in :taille and offre.bois.essence in :essences and commune.id=:communeid")
	Page<Producteur> findByTailleInAndBoisInAndCommune(Pageable pageable,
			@Param("taille") Collection<Integer> tailleList, @Param("essences") Collection<String> essenceList,
			@Param("communeid") Long commune);
}
