package org.fibois38.lbb.service;

import org.fibois38.lbb.service.dto.BoisDTO;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing Bois.
 */
public interface BoisService {

	/**
	 * Save a bois.
	 *
	 * @param boisDTO the entity to save
	 * @return the persisted entity
	 */
	BoisDTO save(BoisDTO boisDTO);

	/**
	 * Get all the bois.
	 *
	 * @return the list of entities
	 */
	List<BoisDTO> findAll();

	/**
	 * Get the "id" bois.
	 *
	 * @param id the id of the entity
	 * @return the entity
	 */
	Optional<BoisDTO> findOne(Long id);

	/**
	 * Delete the "id" bois.
	 *
	 * @param id the id of the entity
	 */
	void delete(Long id);
	
	// Get all essences
	List<String> findDistinctByEssence();
}
