package org.fibois38.lbb.service;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.fibois38.lbb.service.dto.OffreDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing Offre.
 */
public interface OffreService {

	/**
	 * Save a offre.
	 *
	 * @param offreDTO the entity to save
	 * @return the persisted entity
	 */
	OffreDTO save(OffreDTO offreDTO);

	/**
	 * Get all the offres.
	 *
	 * @param pageable the pagination information
	 * @return the list of entities
	 */
	Page<OffreDTO> findAll(Pageable pageable);

	/**
	 * Get all the Offre with eager load of many-to-many relationships.
	 *
	 * @return the list of entities
	 */
	Page<OffreDTO> findAllWithEagerRelationships(Pageable pageable);

	/**
	 * Get the "id" offre.
	 *
	 * @param id the id of the entity
	 * @return the entity
	 */
	Optional<OffreDTO> findOne(Long id);

	/**
	 * Delete the "id" offre.
	 *
	 * @param id the id of the entity
	 */
	void delete(Long id);

	Page<OffreDTO> findByTailleIn(Pageable pageable, Collection<Integer> tailleList);

	Page<OffreDTO> findByTailleInAndBoisIn(Pageable pageable, Collection<Integer> tailleList,
			Collection<String> essenceList);

	Page<OffreDTO> findByTailleInAndBoisInAndCommune(Pageable pageable, Collection<Integer> tailleList,
			Collection<String> essenceList, Long commune);

	// Tailles available
	List<Integer> findDistinctByTaille();
}
