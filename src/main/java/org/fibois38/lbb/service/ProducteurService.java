package org.fibois38.lbb.service;

import org.fibois38.lbb.service.dto.ProducteurDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing Producteur.
 */
public interface ProducteurService {

    /**
     * Save a producteur.
     *
     * @param producteurDTO the entity to save
     * @return the persisted entity
     */
    ProducteurDTO save(ProducteurDTO producteurDTO);

    /**
     * Get all the producteurs.
     *
     * @return the list of entities
     */
    List<ProducteurDTO> findAll();

    /**
     * Get all the Producteur with eager load of many-to-many relationships.
     *
     * @return the list of entities
     */
    Page<ProducteurDTO> findAllWithEagerRelationships(Pageable pageable);
    
    /**
     * Get the "id" producteur.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<ProducteurDTO> findOne(Long id);

    /**
     * Delete the "id" producteur.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
    
	Page<ProducteurDTO> findByTailleInAndBoisInAndCommune(Pageable pageable, Collection<Integer> tailleList,
			Collection<String> essenceList, Long commune);
}
