package org.fibois38.lbb.service.dto;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Lob;

/**
 * A DTO for the Bois entity.
 */
public class BoisDTO implements Serializable {

    private Long id;

    @NotNull
    private String essence;

    @Lob
    private byte[] photo;
    private String photoContentType;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEssence() {
        return essence;
    }

    public void setEssence(String essence) {
        this.essence = essence;
    }

    public byte[] getPhoto() {
        return photo;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

    public String getPhotoContentType() {
        return photoContentType;
    }

    public void setPhotoContentType(String photoContentType) {
        this.photoContentType = photoContentType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        BoisDTO boisDTO = (BoisDTO) o;
        if (boisDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), boisDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "BoisDTO{" +
            "id=" + getId() +
            ", essence='" + getEssence() + "'" +
            ", photo='" + getPhoto() + "'" +
            "}";
    }
}
