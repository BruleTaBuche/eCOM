package org.fibois38.lbb.service.dto;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.Lob;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.fibois38.lbb.domain.enumeration.Frequence;

/**
 * A DTO for the Offre entity.
 */
public class OffreDTO implements Serializable {

    private Long id;

    @NotNull
    private String nom;

    @NotNull
    @DecimalMin(value = "0")
    private Double prix;

    @NotNull
    private Integer taille;

    @NotNull
    private Frequence frequence;

    @Lob
    private String description;

    @NotNull
    @Min(value = 0)
    private Integer stock;

    @NotNull
    private LocalDate dateLivraison;

    @NotNull
    private LocalDate datePublication;

    private Long boisId;
    
    private String boisEssence;

    private Set<CommuneDTO> communes = new HashSet<>();

    private Long producteurId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Double getPrix() {
        return prix;
    }

    public void setPrix(Double prix) {
        this.prix = prix;
    }

    public Integer getTaille() {
        return taille;
    }

    public void setTaille(Integer taille) {
        this.taille = taille;
    }

    public Frequence getFrequence() {
        return frequence;
    }

    public void setFrequence(Frequence frequence) {
        this.frequence = frequence;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    public LocalDate getDateLivraison() {
        return dateLivraison;
    }

    public void setDateLivraison(LocalDate dateLivraison) {
        this.dateLivraison = dateLivraison;
    }

    public LocalDate getDatePublication() {
        return datePublication;
    }

    public void setDatePublication(LocalDate datePublication) {
        this.datePublication = datePublication;
    }

    public Long getBoisId() {
    	return boisId;
    }
    
    public void setBoisId(Long boisId) {
    	this.boisId = boisId;
    }
    
    public String getBoisEssence() {
    	return boisEssence;
    }
    
    public void setBoisEssence(String boisEssence) {
    	this.boisEssence = boisEssence;
    }
    
    public Set<CommuneDTO> getCommunes() {
        return communes;
    }

    public void setCommunes(Set<CommuneDTO> communes) {
        this.communes = communes;
    }

    public Long getProducteurId() {
        return producteurId;
    }

    public void setProducteurId(Long producteurId) {
        this.producteurId = producteurId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        OffreDTO offreDTO = (OffreDTO) o;
        if (offreDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), offreDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "OffreDTO{" +
            "id=" + getId() +
            ", nom='" + getNom() + "'" +
            ", prix=" + getPrix() +
            ", taille=" + getTaille() +
            ", frequence='" + getFrequence() + "'" +
            ", description='" + getDescription() + "'" +
            ", stock=" + getStock() +
            ", dateLivraison='" + getDateLivraison() + "'" +
            ", datePublication='" + getDatePublication() + "'" +
            ", bois='" + getBoisId() + " (" + getBoisEssence() + ")'" +
            ", producteur=" + getProducteurId() +
            "}";
    }
}
