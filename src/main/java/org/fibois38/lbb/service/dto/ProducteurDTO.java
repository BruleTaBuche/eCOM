package org.fibois38.lbb.service.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.Lob;
import javax.validation.constraints.NotNull;

/**
 * A DTO for the Producteur entity.
 */
public class ProducteurDTO implements Serializable {

    private Long id;

    @NotNull
    private String email;

    @Lob
    private byte[] photo;
    private String photoContentType;

    @NotNull
    private String nom;

    @NotNull
    private String prenom;

    
    @Lob
    private String password;

    @Lob
    private String description;

    private Set<CommuneDTO> communes = new HashSet<>();

    private Set<OffreDTO> offres = new HashSet<>();
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public byte[] getPhoto() {
        return photo;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

    public String getPhotoContentType() {
        return photoContentType;
    }

    public void setPhotoContentType(String photoContentType) {
        this.photoContentType = photoContentType;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<CommuneDTO> getCommunes() {
        return communes;
    }

    public void setCommunes(Set<CommuneDTO> communes) {
        this.communes = communes;
    }
    
    public Set<OffreDTO> getOffres() {
        return offres;
    }

    public void setOffres(Set<OffreDTO> offres) {
        this.offres = offres;
    }    

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ProducteurDTO producteurDTO = (ProducteurDTO) o;
        if (producteurDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), producteurDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ProducteurDTO{" +
            "id=" + getId() +
            ", email='" + getEmail() + "'" +
            ", photo='" + getPhoto() + "'" +
            ", nom='" + getNom() + "'" +
            ", prenom='" + getPrenom() + "'" +
            ", password='" + getPassword() + "'" +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
