package org.fibois38.lbb.service.impl;

import org.fibois38.lbb.service.BoisService;
import org.fibois38.lbb.domain.Bois;
import org.fibois38.lbb.repository.BoisRepository;
import org.fibois38.lbb.service.dto.BoisDTO;
import org.fibois38.lbb.service.mapper.BoisMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing Bois.
 */
@Service
@Transactional
public class BoisServiceImpl implements BoisService {

	private final Logger log = LoggerFactory.getLogger(BoisServiceImpl.class);

	private final BoisRepository boisRepository;

	private final BoisMapper boisMapper;

	public BoisServiceImpl(BoisRepository boisRepository, BoisMapper boisMapper) {
		this.boisRepository = boisRepository;
		this.boisMapper = boisMapper;
	}

	/**
	 * Save a bois.
	 *
	 * @param boisDTO the entity to save
	 * @return the persisted entity
	 */
	@Override
	public BoisDTO save(BoisDTO boisDTO) {
		log.debug("Request to save Bois : {}", boisDTO);

		Bois bois = boisMapper.toEntity(boisDTO);
		bois = boisRepository.save(bois);
		return boisMapper.toDto(bois);
	}

	/**
	 * Get all the bois.
	 *
	 * @return the list of entities
	 */
	@Override
	@Transactional(readOnly = true)
	public List<BoisDTO> findAll() {
		log.debug("Request to get all Bois");
		return boisRepository.findAll().stream().map(boisMapper::toDto)
				.collect(Collectors.toCollection(LinkedList::new));
	}

	/**
	 * Get one bois by id.
	 *
	 * @param id the id of the entity
	 * @return the entity
	 */
	@Override
	@Transactional(readOnly = true)
	public Optional<BoisDTO> findOne(Long id) {
		log.debug("Request to get Bois : {}", id);
		return boisRepository.findById(id).map(boisMapper::toDto);
	}

	/**
	 * Delete the bois by id.
	 *
	 * @param id the id of the entity
	 */
	@Override
	public void delete(Long id) {
		log.debug("Request to delete Bois : {}", id);
		boisRepository.deleteById(id);
	}

	@Override
	public List<String> findDistinctByEssence() {
		log.debug("Request to get all essence");
		return boisRepository.findDistinctByEssence();
	}
}
