package org.fibois38.lbb.service.impl;

import org.fibois38.lbb.service.OffreService;
import org.fibois38.lbb.domain.Offre;
import org.fibois38.lbb.repository.OffreRepository;
import org.fibois38.lbb.service.dto.OffreDTO;
import org.fibois38.lbb.service.mapper.OffreMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing Offre.
 */
@Service
@Transactional
public class OffreServiceImpl implements OffreService {

	private final Logger log = LoggerFactory.getLogger(OffreServiceImpl.class);

	private final OffreRepository offreRepository;

	private final OffreMapper offreMapper;

	public OffreServiceImpl(OffreRepository offreRepository, OffreMapper offreMapper) {
		this.offreRepository = offreRepository;
		this.offreMapper = offreMapper;
	}

	/**
	 * Save a offre.
	 *
	 * @param offreDTO the entity to save
	 * @return the persisted entity
	 */
	@Override
	public OffreDTO save(OffreDTO offreDTO) {
		log.debug("Request to save Offre : {}", offreDTO);

		Offre offre = offreMapper.toEntity(offreDTO);
		offre = offreRepository.save(offre);
		return offreMapper.toDto(offre);
	}

	/**
	 * Get all the offres.
	 *
	 * @param pageable the pagination information
	 * @return the list of entities
	 */
	@Override
	@Transactional(readOnly = true)
	public Page<OffreDTO> findAll(Pageable pageable) {
		log.debug("Request to get all Offres");
		return offreRepository.findAll(pageable).map(offreMapper::toDto);
	}

	/**
	 * Get all the Offre with eager load of many-to-many relationships.
	 *
	 * @return the list of entities
	 */
	public Page<OffreDTO> findAllWithEagerRelationships(Pageable pageable) {
		return offreRepository.findAllWithEagerRelationships(pageable).map(offreMapper::toDto);
	}

	/**
	 * Get one offre by id.
	 *
	 * @param id the id of the entity
	 * @return the entity
	 */
	@Override
	@Transactional(readOnly = true)
	public Optional<OffreDTO> findOne(Long id) {
		log.debug("Request to get Offre : {}", id);
		return offreRepository.findOneWithEagerRelationships(id).map(offreMapper::toDto);
	}

	/**
	 * Delete the offre by id.
	 *
	 * @param id the id of the entity
	 */
	@Override
	public void delete(Long id) {
		log.debug("Request to delete Offre : {}", id);
		offreRepository.deleteById(id);
	}

	@Override
	public Page<OffreDTO> findByTailleIn(Pageable pageable, Collection<Integer> tailles) {
		log.debug("Request to get Offres by tailleIn");
		return offreRepository.findByTailleIn(pageable, tailles).map(offreMapper::toDto);
	}

	@Override
	public Page<OffreDTO> findByTailleInAndBoisIn(Pageable pageable, Collection<Integer> tailleList,
			Collection<String> essenceList) {
		log.debug("Request to get Offres by tailleIn, boisIn");
		return offreRepository.findByTailleInAndBoisIn(pageable, tailleList, essenceList).map(offreMapper::toDto);
	}

	@Override
	public Page<OffreDTO> findByTailleInAndBoisInAndCommune(Pageable pageable, Collection<Integer> tailleList,
			Collection<String> essenceList, Long commune) {
		log.debug("Request to get Offres by tailleIn, boisIn, Commune");
		return offreRepository.findByTailleInAndBoisInAndCommune(pageable, tailleList, essenceList, commune)
				.map(offreMapper::toDto);
	}

	@Override
	public List<Integer> findDistinctByTaille() {
		log.debug("Request to get all tailles available");
		return offreRepository.findDistinctByTaille();
	}
}
