package org.fibois38.lbb.service.impl;

import org.fibois38.lbb.service.ProducteurService;
import org.fibois38.lbb.domain.Producteur;
import org.fibois38.lbb.repository.ProducteurRepository;
import org.fibois38.lbb.service.dto.ProducteurDTO;
import org.fibois38.lbb.service.mapper.ProducteurMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing Producteur.
 */
@Service
@Transactional
public class ProducteurServiceImpl implements ProducteurService {

    private final Logger log = LoggerFactory.getLogger(ProducteurServiceImpl.class);

    private final ProducteurRepository producteurRepository;

    private final ProducteurMapper producteurMapper;

    public ProducteurServiceImpl(ProducteurRepository producteurRepository, ProducteurMapper producteurMapper) {
        this.producteurRepository = producteurRepository;
        this.producteurMapper = producteurMapper;
    }

    /**
     * Save a producteur.
     *
     * @param producteurDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public ProducteurDTO save(ProducteurDTO producteurDTO) {
        log.debug("Request to save Producteur : {}", producteurDTO);

        Producteur producteur = producteurMapper.toEntity(producteurDTO);
        producteur = producteurRepository.save(producteur);
        return producteurMapper.toDto(producteur);
    }

    /**
     * Get all the producteurs.
     *
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<ProducteurDTO> findAll() {
        log.debug("Request to get all Producteurs");
        return producteurRepository.findAllWithEagerRelationships().stream()
            .map(producteurMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get all the Producteur with eager load of many-to-many relationships.
     *
     * @return the list of entities
     */
    public Page<ProducteurDTO> findAllWithEagerRelationships(Pageable pageable) {
        return producteurRepository.findAllWithEagerRelationships(pageable).map(producteurMapper::toDto);
    }
    

    /**
     * Get one producteur by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<ProducteurDTO> findOne(Long id) {
        log.debug("Request to get Producteur : {}", id);
        return producteurRepository.findOneWithEagerRelationships(id)
            .map(producteurMapper::toDto);
    }

    /**
     * Delete the producteur by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Producteur : {}", id);
        producteurRepository.deleteById(id);
    }
    
    @Override
	public Page<ProducteurDTO> findByTailleInAndBoisInAndCommune(Pageable pageable, Collection<Integer> tailleList,
			Collection<String> essenceList, Long commune) {
		log.debug("Request to get Producteurs by tailleIn, boisIn, Commune");
		return producteurRepository.findByTailleInAndBoisInAndCommune(pageable, tailleList, essenceList, commune)
				.map(producteurMapper::toDto);
	}
}
