package org.fibois38.lbb.service.mapper;

import org.fibois38.lbb.domain.*;
import org.fibois38.lbb.service.dto.CommandeDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Commande and its DTO CommandeDTO.
 */
@Mapper(componentModel = "spring", uses = {OffreMapper.class, ClientMapper.class})
public interface CommandeMapper extends EntityMapper<CommandeDTO, Commande> {

    @Mapping(source = "offre.id", target = "offreId")
    @Mapping(source = "client.id", target = "clientId")
    CommandeDTO toDto(Commande commande);

    @Mapping(source = "offreId", target = "offre")
    @Mapping(source = "clientId", target = "client")
    Commande toEntity(CommandeDTO commandeDTO);

    default Commande fromId(Long id) {
        if (id == null) {
            return null;
        }
        Commande commande = new Commande();
        commande.setId(id);
        return commande;
    }
}
