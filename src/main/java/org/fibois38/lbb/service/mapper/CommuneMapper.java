package org.fibois38.lbb.service.mapper;

import org.fibois38.lbb.domain.*;
import org.fibois38.lbb.service.dto.CommuneDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Commune and its DTO CommuneDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface CommuneMapper extends EntityMapper<CommuneDTO, Commune> {


    @Mapping(target = "producteurs", ignore = true)
    @Mapping(target = "offres", ignore = true)
    @Mapping(target = "clients", ignore = true)
    Commune toEntity(CommuneDTO communeDTO);

    default Commune fromId(Long id) {
        if (id == null) {
            return null;
        }
        Commune commune = new Commune();
        commune.setId(id);
        return commune;
    }
}
