package org.fibois38.lbb.service.mapper;

import org.fibois38.lbb.domain.*;
import org.fibois38.lbb.service.dto.OffreDTO;
import org.fibois38.lbb.service.dto.BoisDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Offre and its DTO OffreDTO.
 */
@Mapper(componentModel = "spring", uses = {BoisMapper.class, CommuneMapper.class, ProducteurMapper.class})
public interface OffreMapper extends EntityMapper<OffreDTO, Offre> {

	@Mapping(source = "bois.id", target = "boisId")
	@Mapping(source = "bois.essence", target = "boisEssence")
    @Mapping(source = "producteur.id", target = "producteurId")
    OffreDTO toDto(Offre offre);

	@Mapping(source = "boisId", target = "bois")
    @Mapping(source = "producteurId", target = "producteur")
    @Mapping(target = "commandes", ignore = true)
    Offre toEntity(OffreDTO offreDTO);

    default Offre fromId(Long id) {
        if (id == null) {
            return null;
        }
        Offre offre = new Offre();
        offre.setId(id);
        return offre;
    }
}
