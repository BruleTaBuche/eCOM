package org.fibois38.lbb.service.mapper;

import org.fibois38.lbb.domain.*;
import org.fibois38.lbb.service.dto.ProducteurDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Producteur and its DTO ProducteurDTO.
 */
@Mapper(componentModel = "spring", uses = {CommuneMapper.class, OffreMapper.class})
public interface ProducteurMapper extends EntityMapper<ProducteurDTO, Producteur> {


    //@Mapping(target = "offres", ignore = true)
    Producteur toEntity(ProducteurDTO producteurDTO);

    default Producteur fromId(Long id) {
        if (id == null) {
            return null;
        }
        Producteur producteur = new Producteur();
        producteur.setId(id);
        return producteur;
    }
}
