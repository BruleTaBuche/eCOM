package org.fibois38.lbb.web.rest;

import com.codahale.metrics.annotation.Timed;
import org.fibois38.lbb.service.BoisService;
import org.fibois38.lbb.web.rest.errors.BadRequestAlertException;
import org.fibois38.lbb.web.rest.util.HeaderUtil;
import org.fibois38.lbb.service.dto.BoisDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Bois.
 */
@RestController
@RequestMapping("/api")
public class BoisResource {

	private final Logger log = LoggerFactory.getLogger(BoisResource.class);

	private static final String ENTITY_NAME = "bois";

	private final BoisService boisService;

	public BoisResource(BoisService boisService) {
		this.boisService = boisService;
	}

	/**
	 * POST /bois : Create a new bois.
	 *
	 * @param boisDTO the boisDTO to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         boisDTO, or with status 400 (Bad Request) if the bois has already an
	 *         ID
	 * @throws URISyntaxException if the Location URI syntax is incorrect
	 */
	@PostMapping("/bois")
	@Timed
	public ResponseEntity<BoisDTO> createBois(@Valid @RequestBody BoisDTO boisDTO) throws URISyntaxException {
		log.debug("REST request to save Bois : {}", boisDTO);
		if (boisDTO.getId() != null) {
			throw new BadRequestAlertException("A new bois cannot already have an ID", ENTITY_NAME, "idexists");
		}
		BoisDTO result = boisService.save(boisDTO);
		return ResponseEntity.created(new URI("/api/bois/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
	}

	/**
	 * PUT /bois : Updates an existing bois.
	 *
	 * @param boisDTO the boisDTO to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         boisDTO, or with status 400 (Bad Request) if the boisDTO is not
	 *         valid, or with status 500 (Internal Server Error) if the boisDTO
	 *         couldn't be updated
	 * @throws URISyntaxException if the Location URI syntax is incorrect
	 */
	@PutMapping("/bois")
	@Timed
	public ResponseEntity<BoisDTO> updateBois(@Valid @RequestBody BoisDTO boisDTO) throws URISyntaxException {
		log.debug("REST request to update Bois : {}", boisDTO);
		if (boisDTO.getId() == null) {
			throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
		}
		BoisDTO result = boisService.save(boisDTO);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, boisDTO.getId().toString()))
				.body(result);
	}

	/**
	 * GET /bois : get all the bois.
	 *
	 * @return the ResponseEntity with status 200 (OK) and the list of bois in body
	 */
	@GetMapping("/bois")
	@Timed
	public List<BoisDTO> getAllBois() {
		log.debug("REST request to get all Bois");
		return boisService.findAll();
	}

	/**
	 * GET /bois/:id : get the "id" bois.
	 *
	 * @param id the id of the boisDTO to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the boisDTO, or
	 *         with status 404 (Not Found)
	 */
	@GetMapping("/bois/{id}")
	@Timed
	public ResponseEntity<BoisDTO> getBois(@PathVariable Long id) {
		log.debug("REST request to get Bois : {}", id);
		Optional<BoisDTO> boisDTO = boisService.findOne(id);
		return ResponseUtil.wrapOrNotFound(boisDTO);
	}

	/**
	 * DELETE /bois/:id : delete the "id" bois.
	 *
	 * @param id the id of the boisDTO to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/bois/{id}")
	@Timed
	public ResponseEntity<Void> deleteBois(@PathVariable Long id) {
		log.debug("REST request to delete Bois : {}", id);
		boisService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
	}

	@GetMapping("/bois/essences")
	@Timed
	public List<String> getEssence() {
		List<String> essenceList = boisService.findDistinctByEssence();
		log.debug("REST request to get essence");
		log.debug("Liste essence : {}", essenceList);
		return essenceList;
	}
}
