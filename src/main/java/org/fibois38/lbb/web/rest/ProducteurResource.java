package org.fibois38.lbb.web.rest;

import com.codahale.metrics.annotation.Timed;
import org.fibois38.lbb.service.ProducteurService;
import org.fibois38.lbb.web.rest.errors.BadRequestAlertException;
import org.fibois38.lbb.web.rest.util.HeaderUtil;
import org.fibois38.lbb.web.rest.util.PaginationUtil;
import org.fibois38.lbb.service.dto.ProducteurDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Producteur.
 */
@RestController
@RequestMapping("/api")
public class ProducteurResource {

	private final Logger log = LoggerFactory.getLogger(ProducteurResource.class);

	private static final String ENTITY_NAME = "producteur";

	private final ProducteurService producteurService;

	public ProducteurResource(ProducteurService producteurService) {
		this.producteurService = producteurService;
	}

	/**
	 * POST /producteurs : Create a new producteur.
	 *
	 * @param producteurDTO the producteurDTO to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         producteurDTO, or with status 400 (Bad Request) if the producteur has
	 *         already an ID
	 * @throws URISyntaxException if the Location URI syntax is incorrect
	 */
	@PostMapping("/producteurs")
	@Timed
	public ResponseEntity<ProducteurDTO> createProducteur(@Valid @RequestBody ProducteurDTO producteurDTO)
			throws URISyntaxException {
		log.debug("REST request to save Producteur : {}", producteurDTO);
		if (producteurDTO.getId() != null) {
			throw new BadRequestAlertException("A new producteur cannot already have an ID", ENTITY_NAME, "idexists");
		}
		ProducteurDTO result = producteurService.save(producteurDTO);
		return ResponseEntity.created(new URI("/api/producteurs/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
	}

	/**
	 * PUT /producteurs : Updates an existing producteur.
	 *
	 * @param producteurDTO the producteurDTO to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         producteurDTO, or with status 400 (Bad Request) if the producteurDTO
	 *         is not valid, or with status 500 (Internal Server Error) if the
	 *         producteurDTO couldn't be updated
	 * @throws URISyntaxException if the Location URI syntax is incorrect
	 */
	@PutMapping("/producteurs")
	@Timed
	public ResponseEntity<ProducteurDTO> updateProducteur(@Valid @RequestBody ProducteurDTO producteurDTO)
			throws URISyntaxException {
		log.debug("REST request to update Producteur : {}", producteurDTO);
		if (producteurDTO.getId() == null) {
			throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
		}
		ProducteurDTO result = producteurService.save(producteurDTO);
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, producteurDTO.getId().toString()))
				.body(result);
	}

	/**
	 * GET /producteurs : get all the producteurs.
	 *
	 * @param eagerload flag to eager load entities from relationships (This is
	 *                  applicable for many-to-many)
	 * @return the ResponseEntity with status 200 (OK) and the list of producteurs
	 *         in body
	 */
	@GetMapping("/producteurs")
	@Timed
	public List<ProducteurDTO> getAllProducteurs(
			@RequestParam(required = false, defaultValue = "false") boolean eagerload) {
		log.debug("REST request to get all Producteurs");
		return producteurService.findAll();
	}

	/**
	 * GET /producteurs/:id : get the "id" producteur.
	 *
	 * @param id the id of the producteurDTO to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         producteurDTO, or with status 404 (Not Found)
	 */
	@GetMapping("/producteurs/{id}")
	@Timed
	public ResponseEntity<ProducteurDTO> getProducteur(@PathVariable Long id) {
		log.debug("REST request to get Producteur : {}", id);
		Optional<ProducteurDTO> producteurDTO = producteurService.findOne(id);
		return ResponseUtil.wrapOrNotFound(producteurDTO);
	}

	/**
	 * DELETE /producteurs/:id : delete the "id" producteur.
	 *
	 * @param id the id of the producteurDTO to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/producteurs/{id}")
	@Timed
	public ResponseEntity<Void> deleteProducteur(@PathVariable Long id) {
		log.debug("REST request to delete Producteur : {}", id);
		producteurService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
	}

	@GetMapping("/producteurs/matchTailleEtBoisEtCommune")
	@Timed
	public ResponseEntity<List<ProducteurDTO>> getProducteur(Pageable pageable,
			@RequestParam(required = true) String tailles, @RequestParam(required = true) String essences,
			@RequestParam(required = true) Long commune) {
		log.debug("REST request to get a page of Offres");
		Page<ProducteurDTO> page;
		List<Integer> tailleList = new ArrayList<Integer>();
		for (String taille : tailles.split(","))
			tailleList.add(new Integer(taille));

		List<String> essenceList = new ArrayList<String>();
		for (String essence : essences.split(","))
			essenceList.add(essence);

		page = producteurService.findByTailleInAndBoisInAndCommune(pageable, tailleList, essenceList, commune);

		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page,
				String.format("/api/producteur?eagerload=%b", true));
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}
}
