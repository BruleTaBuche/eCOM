/**
 * View Models used by Spring MVC REST controllers.
 */
package org.fibois38.lbb.web.rest.vm;
