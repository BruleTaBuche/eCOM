import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { ResultComponent } from './result/result.component';
import { ProducerDetailsComponent } from './producer-details/producer-details.component';

import { btbRoute } from './btb.route';
import { ProducerInformationComponent } from './producer-details/producer-information/producer-information.component';
import { ServiceComponent } from './producer-details/propositions/services-list/service/service.component';
import { ServicesListComponent } from './producer-details/propositions/services-list/services-list.component';
import { OfferComponent } from './producer-details/propositions/offers-list/offer/offer.component';
import { OffersListComponent } from './producer-details/propositions/offers-list/offers-list.component';
import { PropositionsComponent } from './producer-details/propositions/propositions.component';
import { WoodTypesListComponent } from './producer-details/propositions/wood-types-list/wood-types-list.component';
import { BtbSharedModule } from 'app/shared';

@NgModule({
    imports: [BtbSharedModule, CommonModule, RouterModule.forChild(btbRoute)],
    declarations: [
        ResultComponent,
        ProducerDetailsComponent,
        ProducerInformationComponent,
        ServiceComponent,
        ServicesListComponent,
        OfferComponent,
        OffersListComponent,
        PropositionsComponent,
        WoodTypesListComponent
    ]
})
export class BtbModule { }
