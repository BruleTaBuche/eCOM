import { Routes } from '@angular/router';

import { resultRoute } from '../btb/result/result.route';
import { producerDetailsRoute } from '../btb/producer-details/producer-details.route';

const CHILDREN_ROUTES = [resultRoute, producerDetailsRoute];

export const btbRoute: Routes = [
    {
        path: '',
        children: CHILDREN_ROUTES
    }
];
