import { Component, OnInit } from '@angular/core';
import { Producer } from '../shared/models/producer.model';
import { ActivatedRoute } from '@angular/router';
import { ProducerService } from '../shared/services/producer.service';

@Component({
    selector: 'jhi-producer-details',
    templateUrl: './producer-details.component.html',
    styles: []
})
export class ProducerDetailsComponent implements OnInit {

    public producer: Producer;

    constructor(
        private route: ActivatedRoute,
        private producerService: ProducerService
    ) { }

    ngOnInit() {
        this.getProducer();
    }

    /**
     * Extract the '_id' key from the URL and get its producer
     */
    private getProducer() {
        // '+' is for string to number cast
        const id: number = + this.route.snapshot.paramMap.get('id');
        this.producerService.getProducer(id).subscribe(producer => {
            this.producer = producer;
        });
    }
}
