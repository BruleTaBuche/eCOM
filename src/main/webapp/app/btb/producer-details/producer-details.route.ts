import { Route } from '@angular/router';
import { ProducerDetailsComponent } from './producer-details.component';

export const producerDetailsRoute: Route = {
    path: 'detailProducteur/:id',
    component: ProducerDetailsComponent
};
