import { Component, OnInit, Input } from '@angular/core';
import { Producer } from '../../shared/models/producer.model';

@Component({
    selector: 'jhi-producer-information',
    templateUrl: './producer-information.component.html',
    styles: []
})
export class ProducerInformationComponent implements OnInit {

    // Get producer from the producer-details component
    @Input() producer: Producer;

    constructor() { }

    ngOnInit() { }

}
