import { Component, OnInit, Input } from '@angular/core';
import { Producer } from '../../../shared/models/producer.model';
import { Offer } from '../../../shared/models/offer.model';

@Component({
    selector: 'jhi-offers-list',
    templateUrl: './offers-list.component.html',
    styles: []
})
export class OffersListComponent implements OnInit {

    @Input() producer: Producer; // get the producer from the propositions component

    offers: Offer[];

    constructor() { }

    ngOnInit() {
        this.getOffers(this.producer);
    }

    /**
     * Get the offers proposed by the producer
     * @param producer
     */
    getOffers(producer) {
        this.offers = producer.getOffers();
    }

}
