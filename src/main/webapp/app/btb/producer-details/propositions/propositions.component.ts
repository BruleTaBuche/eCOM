import { Component, OnInit, Input } from '@angular/core';
import { Producer } from '../../shared/models/producer.model';

@Component({
    selector: 'jhi-propositions',
    templateUrl: './propositions.component.html',
    styles: []
})
export class PropositionsComponent implements OnInit {

    @Input() producer: Producer; // get the producer from the producer-details component

    constructor() { }

    ngOnInit() { }

}
