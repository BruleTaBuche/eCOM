import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { CommuneService } from '../shared/services/commune.service';
import { ProducerService } from '../shared/services/producer.service';
import { SpeciesService } from '../shared/services/species.service';
import { SizeService } from '../shared/services/size.service';

import { Commune } from '../shared/models/commune.model';
import { Offer } from '../shared/models/offer.model';
import { Producer } from '../shared/models/producer.model';
import { Species } from '../shared/models/species.model';

@Component({
    selector: 'jhi-result',
    templateUrl: './result.component.html',
    styles: []
})
export class ResultComponent implements OnInit {

    /**
     * Whether the research panel is open
     */
    panelOpenState = true;

    /**
     * The currently selected commune
     */
    selectedCommune: Commune;

    /**
     * Every available commune
     */
    _communes: Commune[] = [];

    /**
     * Every pickable size and its "checked" state
     */
    _sizes: [number, boolean][] = [];

    /**
     * Every pickable species and its "checked" state
     */
    _species: [string, boolean][] = [];

    /**
     * Every producer having at leat one offer matching the given criterias
     */
    _matchingProducers: Producer[];

    constructor(
        private sizeService: SizeService,
        private speciesService: SpeciesService,
        private producerService: ProducerService,
        private communeService: CommuneService,
        private router: Router,
    ) {

    }

    ngOnInit() {

        // FIXME : add research button

        this.getAvailableSizes();
        this.getAvailableSpecies();
        this.getMatchingProducers();
        this.getAvailableCommunes();
    }

    /**
     * Subscribes to the (communeService's) availableCommunes observable in order to keep communes always up-to-date and sorted
     */
    private getAvailableCommunes(): void {

        this.communeService.getAvailableCommunes().subscribe(avCommunes => {

            avCommunes.forEach(commune => {
                if (!this._communes.includes(commune)) {
                    this._communes.push(commune);
                }
            });
            // sort this._communes for display purpose
            this._communes.sort((commune1, commune2) => commune1.getZIPCode() - commune2.getZIPCode());
        });

    }

    /**
     * Subscribes to the (sizeService's) availableSizes observable in order to keep sizes always up-to-date and sorted
     */
    private getAvailableSizes(): void {
        // On new version of availables sizes, add <nb, true> if nb is not already in this.sizes
        this.sizeService.getAvailableSizes().subscribe(avSizes => {
            avSizes.forEach(size => {
                if (!this.inSizes(size)) {
                    this._sizes.push([size, true]);
                }
            });
            // sort this.sizes for display purpose
            this._sizes.sort((el1, el2) => el1[0] - el2[0]);
        });
    }

    /**
     * Returns true if this.sizes has a member with size parameter value as first property
     * Examples :
     * - this.sizes = [<25, false>, <33, true>, <50, false>] and size = 33 ==> returns true
     * - this.sizes = [<25, false>, <33, true>, <50, false>] and size = 98 ==> returns false
     * @param size the number to test all this.sizes elements first property against
     */
    private inSizes(size: number): boolean {
        return this._sizes ? this._sizes.some(element => element[0] === size) : false;
    }

    /**
     * Subscribes to the (speciesService's) availableSpecies observable in order to keep species always up-to-date and sorted
     */
    private getAvailableSpecies(): void {
        // On new version of availables species, add <species, true> if species is not already in this.species
        this.speciesService.getAvailableSpecies().subscribe(avSpecies => {
            avSpecies.forEach(species => {
                if (!this.inSpecies(species)) {
                    this._species.push([species, true]);
                }
            });
            // sort this.sizes for display purpose
            this._species.sort();
        });
    }

    /**
     * Returns true if this.species has a member with species parameter value as first property
     * Examples :
     * - this.sizes = [<'Hetre', false>, <'Chene', true>, <'Frene', true>] and species = Chene ==> returns true
     * - this.sizes = [<'Hetre', false>, <'Frene', true>] and species = Chene ==> returns true
     * @param species the species (as string) to test all this.species elements first property against
     */
    private inSpecies(species: string): boolean {
        return this._species ? this._species.some(element => element[0] === species) : false;
    }

    /**
     * Subscibes to an observable that lists the producers that have at least an offer matching the criterias given in input
     */
    getMatchingProducers(): void {

        const selectedSizes: number[] = this._sizes.filter(size => size[1]).map(size => size[0]);
        const selectedSpecies: Species[] = this._species.filter(species => species[1]).map(selectedSp => Species[selectedSp[0]]);

        if (!(selectedSizes === undefined || selectedSizes.length === 0) && !(selectedSpecies === undefined || selectedSpecies.length === 0)) {
            this.producerService.getMatchingProducers(this.selectedCommune, selectedSizes, selectedSpecies).subscribe(producers => {
                this._matchingProducers = producers;
            });
        }

    }

    public getMatchingOffers(producer: Producer): Offer[] {
        const selectedSizes: number[] = this._sizes.filter(size => size[1]).map(selectedSize => selectedSize[0]);
        const selectedSpecies: Species[] = this._species.filter(species => species[1]).map(selectedSp => Species[selectedSp[0]]);

        const originalOfferlist: Offer[] = producer.getOffers();
        const offerlist: Offer[] = producer.getOffers().filter(offer => offer.matches(this.selectedCommune, selectedSizes, selectedSpecies));

        return offerlist;
    }

    public onClickProducer(prod: Producer): void {
        this.router.navigateByUrl('/detailProducteur/' + prod.getId());
    }

    // ----------------------------------------- //
    // ---------- Getters and Setters ---------- //
    // ----------------------------------------- //

    public getSizes(): [number, boolean][] {
        return this._sizes;
    }

    public getSpecies(): [string, boolean][] {
        return this._species;
    }

    public getCommunes(): Commune[] {
        return this._communes;
    }

}
