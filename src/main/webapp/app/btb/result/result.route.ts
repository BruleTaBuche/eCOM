import { Route } from '@angular/router';
import { ResultComponent } from './result.component';

export const resultRoute: Route = {
    path: 'recherche',
    component: ResultComponent
};
