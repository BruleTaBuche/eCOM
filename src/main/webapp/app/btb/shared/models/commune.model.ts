export class Commune {

    static buildFromApi(communeAsAny: any): Commune {
        return new Commune(
            communeAsAny['id'],
            communeAsAny['codePostal'],
            communeAsAny['nom']
        );
    }

    constructor(
        private id: number,
        private _ZIPCode: number,
        private _name: string) { }

    /**
     * toString
     */
    public toString(): string {
        return this._ZIPCode + ' - ' + this._name;
    }

    public equals(otherCommune: Commune): boolean {
        return this.getId() === otherCommune.getId() && this.getZIPCode() === otherCommune.getZIPCode() && this.getName() === otherCommune.getName();
    }

    // -------------------------
    // -- Getters and setters --
    // -------------------------

    public getId(): number {
        return this.id;
    }

    public getZIPCode(): number {
        return this._ZIPCode;
    }

    public getName(): string {
        return this._name;
    }
}
