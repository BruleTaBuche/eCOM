export enum Frequency {
    'Ponctuel',
    'Quotidien',
    'Hebdomadaire',
    'Mensuel'
}
