import { Commune } from './commune.model';
import { Frequency } from './frequency.model';
import { Producer } from './producer.model';
import { Species } from './species.model';

export class Offer {

    public static buildFromApi(offerAsAny: any): Offer {

        return new Offer(
            Species[offerAsAny['boisEssence'] as keyof typeof Species],
            offerAsAny['nom'],
            offerAsAny['prix'],
            offerAsAny['taille'],
            offerAsAny['stock'],
            (offerAsAny['communes'] as Commune[]).map(communeAsAny => Commune.buildFromApi(communeAsAny)),
            Offer['frequence'],
            Offer['description']);
    }

    constructor(
        private _species: Species,
        private _name: string,
        private _price: number,
        private _size: number,
        private _quantity: number,
        private _communes: Commune[],
        private _frequency: Frequency,
        private _description: string
    ) {

    }

    /**
     * Returns true if the offer matches the given criterias
     * @param species The wood species the offer has to be
     * @param size The wood size the offer has to be
     * @param commune The commune the offer has to be in
     */
    public matches(commune: Commune, sizes: number[], species: Species[]): boolean {
        return this._communes.map(comm => comm.equals(commune)) && sizes.includes(this._size) && species.includes(this._species);
    }

    // --- Getters and setters --- //

    public getName(): string {
        return this._name;
    }

    public getSpecies(): Species {
        return this._species;
    }

    public getSpeciesAsString(): string {
        return Species[this._species];
    }

    public getCommunes(): Commune[] {
        return this._communes;
    }

    public getSize(): number {
        return this._size;
    }

    public getQuantity(): number {
        return this._quantity;
    }
}
