import { Offer } from './offer.model';

export class Producer {

    static buildFromAPI(producer: any): Producer {
        return new Producer(
            producer['id'],
            producer['nom'],
            producer['photo'],
            producer['description'],
            producer['offres'].map(offerAsAny => Offer.buildFromApi(offerAsAny)),
        );
    }

    constructor(
        private _id: number,
        private _name: string,
        private _image: string,
        private _infos: string,
        private _offers: Offer[]) {

    }

    // --- Getters and setters --- //

    public getId(): number {
        return this._id;
    }

    public getName(): string {
        return this._name;
    }

    public getImage(): string {
        return this._image;
    }

    public getInfos(): string {
        return this._infos;
    }

    public getOffers(): Offer[] {
        return this._offers;
    }
}
