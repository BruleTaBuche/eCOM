import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { Commune } from '../models/commune.model';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CommuneService {

  private allCommunesUrl: string = 'api/communes';

  constructor(
    private http: HttpClient,
  ) { }

  public getAvailableCommunes(): Observable<Commune[]> {

    return this.http.get<any[]>(this.allCommunesUrl).pipe(
      map(communesAsObject =>
        communesAsObject.map(commune =>
          new Commune(commune['id'], commune['codePostal'], commune['nom'])
        )
      )
    );
  }
}
