import { Injectable } from '@angular/core';
import { HttpHeaders, HttpParams, HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { Commune } from '../models/commune.model';
import { Offer } from '../models/offer.model';
import { Species } from '../models/species.model';

@Injectable({
    providedIn: 'root'
})
export class OfferService {

    private matchingOffersUrl: string = 'api/offres/matchTailleEtBoisEtCommune';

    constructor(
        private http: HttpClient,
    ) { }

    public getMatchingOffers(commune: Commune, sizes: number[], species: Species[]): Observable<Offer[]> {

        if (!commune) {
            return of([]);
        }

        const headers = new HttpHeaders();
        headers.append('Content-Type', 'application/json');
        headers.append('projectid', '1');

        const params = new HttpParams()
            .set('tailles', sizes.join())
            .set('essences', species.join())
            .set('commune', commune.getId().toString());

        return this.http.get<Offer[]>(this.matchingOffersUrl, { headers: headers, params: params });

    }
}
