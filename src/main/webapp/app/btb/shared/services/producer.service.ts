import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { Commune } from '../models/commune.model';
import { Producer } from '../models/producer.model';
import { Species } from '../models/species.model';

@Injectable({
    providedIn: 'root'
})
export class ProducerService {
    private matchingProducersUrl: string = 'api/producteurs/matchTailleEtBoisEtCommune';

    constructor(private http: HttpClient) { }

    /**
     * Returns an observable of all the producers having at least one offer matching the given criterias
     * @param commune The commune to match
     * @param sizes The sizes the offer has to have
     * @param species The species the offer has to have
     */
    public getMatchingProducers(commune: Commune, sizes: number[], species: Species[]): Observable<Producer[]> {
        if (!commune) {
            return of([]);
        }

        const headers = new HttpHeaders();
        headers.append('Content-Type', 'application/json');
        headers.append('projectid', '1');

        const speciesAsName: string[] = [];
        for (const sp of Object.keys(species)) {
            speciesAsName.push(Species[sp]);
        }

        const params = new HttpParams()
            .set('tailles', sizes.join())
            .set('essences', speciesAsName.join())
            .set('commune', commune.getId().toString());

        return this.http.get<any[]>(this.matchingProducersUrl, { headers: headers, params: params }).pipe(
            map(producersAsObject => {
                return producersAsObject.map(producer => Producer.buildFromAPI(producer));
            })
        );
    }

    /**
     * Returns the producer whose id is "id"
     * @param id
     */
    public getProducer(id: number): Observable<Producer> {
        const producerUrl: string = 'api/producteurs/' + id;

        return this.http.get<any>(producerUrl).pipe(
            map(producerAsObject => {
                return Producer.buildFromAPI(producerAsObject);
            }));
    }

    /**
     * Handle Http operation that failed.
     * Let the app continue.
     * @param operation - name of the operation that failed
     * @param result - optional value to return as the observable result
     */
    private handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {
            // TODO: send the error to remote logging infrastructure
            console.error(error); // log to console instead

            // Let the app keep running by returning an empty result.
            return of(result as T);
        };
    }
}
