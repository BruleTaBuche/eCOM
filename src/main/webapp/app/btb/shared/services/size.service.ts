import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

import { HttpClient } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class SizeService {

    private allSizesUrl: string = 'api/offres/tailles';

    constructor(
        private http: HttpClient,
    ) { }

    public getAvailableSizes(): Observable<number[]> {
        return this.http.get<number[]>(this.allSizesUrl);
    }
}
