import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class SpeciesService {

    private allSpeciesUrl: string = 'api/bois/essences';

    constructor(
        private http: HttpClient,
    ) { }

    public getAvailableSpecies(): Observable<string[]> {
        return this.http.get<string[]>(this.allSpeciesUrl);
    }
}
