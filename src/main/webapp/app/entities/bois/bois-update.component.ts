import { Component, OnInit, ElementRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { JhiDataUtils } from 'ng-jhipster';

import { IBois } from 'app/shared/model/bois.model';
import { BoisService } from './bois.service';

@Component({
    selector: 'jhi-bois-update',
    templateUrl: './bois-update.component.html'
})
export class BoisUpdateComponent implements OnInit {
    bois: IBois;
    isSaving: boolean;

    constructor(
        private dataUtils: JhiDataUtils,
        private boisService: BoisService,
        private elementRef: ElementRef,
        private activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ bois }) => {
            this.bois = bois;
        });
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }

    setFileData(event, entity, field, isImage) {
        this.dataUtils.setFileData(event, entity, field, isImage);
    }

    clearInputImage(field: string, fieldContentType: string, idInput: string) {
        this.dataUtils.clearInputImage(this.bois, this.elementRef, field, fieldContentType, idInput);
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.bois.id !== undefined) {
            this.subscribeToSaveResponse(this.boisService.update(this.bois));
        } else {
            this.subscribeToSaveResponse(this.boisService.create(this.bois));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IBois>>) {
        result.subscribe((res: HttpResponse<IBois>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }
}
