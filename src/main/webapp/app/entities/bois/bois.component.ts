import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiAlertService, JhiDataUtils } from 'ng-jhipster';

import { IBois } from 'app/shared/model/bois.model';
import { Principal } from 'app/core';
import { BoisService } from './bois.service';

@Component({
    selector: 'jhi-bois',
    templateUrl: './bois.component.html'
})
export class BoisComponent implements OnInit, OnDestroy {
    bois: IBois[];
    currentAccount: any;
    eventSubscriber: Subscription;
    displayedColumns: string[] = ['id', 'essence', 'photo', 'actions'];

    constructor(
        private boisService: BoisService,
        private jhiAlertService: JhiAlertService,
        private dataUtils: JhiDataUtils,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {}

    loadAll() {
        this.boisService.query().subscribe(
            (res: HttpResponse<IBois[]>) => {
                this.bois = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInBois();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: IBois) {
        return item.id;
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }

    registerChangeInBois() {
        this.eventSubscriber = this.eventManager.subscribe('boisListModification', response => this.loadAll());
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
