import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { Bois } from 'app/shared/model/bois.model';
import { BoisService } from './bois.service';
import { BoisComponent } from './bois.component';
import { BoisDetailComponent } from './bois-detail.component';
import { BoisUpdateComponent } from './bois-update.component';
import { BoisDeletePopupComponent } from './bois-delete-dialog.component';
import { IBois } from 'app/shared/model/bois.model';

@Injectable({ providedIn: 'root' })
export class BoisResolve implements Resolve<IBois> {
    constructor(private service: BoisService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(map((bois: HttpResponse<Bois>) => bois.body));
        }
        return of(new Bois());
    }
}

export const boisRoute: Routes = [
    {
        path: 'bois',
        component: BoisComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'btbApp.bois.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'bois/:id/view',
        component: BoisDetailComponent,
        resolve: {
            bois: BoisResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'btbApp.bois.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'bois/new',
        component: BoisUpdateComponent,
        resolve: {
            bois: BoisResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'btbApp.bois.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'bois/:id/edit',
        component: BoisUpdateComponent,
        resolve: {
            bois: BoisResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'btbApp.bois.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const boisPopupRoute: Routes = [
    {
        path: 'bois/:id/delete',
        component: BoisDeletePopupComponent,
        resolve: {
            bois: BoisResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'btbApp.bois.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
