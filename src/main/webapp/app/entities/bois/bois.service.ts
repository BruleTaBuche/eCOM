import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IBois } from 'app/shared/model/bois.model';

type EntityResponseType = HttpResponse<IBois>;
type EntityArrayResponseType = HttpResponse<IBois[]>;

@Injectable({ providedIn: 'root' })
export class BoisService {
    private resourceUrl = SERVER_API_URL + 'api/bois';

    constructor(private http: HttpClient) {}

    create(bois: IBois): Observable<EntityResponseType> {
        return this.http.post<IBois>(this.resourceUrl, bois, { observe: 'response' });
    }

    update(bois: IBois): Observable<EntityResponseType> {
        return this.http.put<IBois>(this.resourceUrl, bois, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<IBois>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IBois[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
