export * from './bois.service';
export * from './bois-update.component';
export * from './bois-delete-dialog.component';
export * from './bois-detail.component';
export * from './bois.component';
export * from './bois.route';
