import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ICommande } from 'app/shared/model/commande.model';
import { Principal } from 'app/core';
import { CommandeService } from './commande.service';

@Component({
    selector: 'jhi-commande',
    templateUrl: './commande.component.html'
})
export class CommandeComponent implements OnInit, OnDestroy {
    commandes: ICommande[];
    currentAccount: any;
    eventSubscriber: Subscription;
    displayedColumns: string[] = ['id', 'date', 'prix', 'etat', 'offre', 'client', 'actions'];

    constructor(
        private commandeService: CommandeService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {}

    loadAll() {
        this.commandeService.query().subscribe(
            (res: HttpResponse<ICommande[]>) => {
                this.commandes = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInCommandes();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: ICommande) {
        return item.id;
    }

    registerChangeInCommandes() {
        this.eventSubscriber = this.eventManager.subscribe('commandeListModification', response => this.loadAll());
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
