import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ICommune } from 'app/shared/model/commune.model';
import { Principal } from 'app/core';
import { CommuneService } from './commune.service';

@Component({
    selector: 'jhi-commune',
    templateUrl: './commune.component.html'
})
export class CommuneComponent implements OnInit, OnDestroy {
    communes: ICommune[];
    currentAccount: any;
    eventSubscriber: Subscription;
    displayedColumns: string[] = ['id', 'nom', 'codePostal', 'actions'];

    constructor(
        private communeService: CommuneService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {}

    loadAll() {
        this.communeService.query().subscribe(
            (res: HttpResponse<ICommune[]>) => {
                this.communes = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInCommunes();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: ICommune) {
        return item.id;
    }

    registerChangeInCommunes() {
        this.eventSubscriber = this.eventManager.subscribe('communeListModification', response => this.loadAll());
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
