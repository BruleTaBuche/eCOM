import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { BtbClientModule } from './client/client.module';
import { BtbProducteurModule } from './producteur/producteur.module';
import { BtbOffreModule } from './offre/offre.module';
import { BtbCommuneModule } from './commune/commune.module';
import { BtbBoisModule } from './bois/bois.module';
import { BtbCommandeModule } from './commande/commande.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    // prettier-ignore
    imports: [
        BtbClientModule,
        BtbProducteurModule,
        BtbOffreModule,
        BtbCommuneModule,
        BtbBoisModule,
        BtbCommandeModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class BtbEntityModule {}
