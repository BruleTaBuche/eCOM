import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { JhiDataUtils } from 'ng-jhipster';

import { IProducteur } from 'app/shared/model/producteur.model';

@Component({
    selector: 'jhi-producteur-detail',
    templateUrl: './producteur-detail.component.html'
})
export class ProducteurDetailComponent implements OnInit {
    producteur: IProducteur;

    constructor(private dataUtils: JhiDataUtils, private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ producteur }) => {
            this.producteur = producteur;
        });
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }
    previousState() {
        window.history.back();
    }
}
