import { Component, OnInit, ElementRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { JhiAlertService, JhiDataUtils } from 'ng-jhipster';

import { IProducteur } from 'app/shared/model/producteur.model';
import { ProducteurService } from './producteur.service';
import { ICommune } from 'app/shared/model/commune.model';
import { CommuneService } from 'app/entities/commune';

@Component({
    selector: 'jhi-producteur-update',
    templateUrl: './producteur-update.component.html'
})
export class ProducteurUpdateComponent implements OnInit {
    producteur: IProducteur;
    isSaving: boolean;

    communes: ICommune[];

    constructor(
        private dataUtils: JhiDataUtils,
        private jhiAlertService: JhiAlertService,
        private producteurService: ProducteurService,
        private communeService: CommuneService,
        private elementRef: ElementRef,
        private activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ producteur }) => {
            this.producteur = producteur;
        });
        this.communeService.query().subscribe(
            (res: HttpResponse<ICommune[]>) => {
                this.communes = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }

    setFileData(event, entity, field, isImage) {
        this.dataUtils.setFileData(event, entity, field, isImage);
    }

    clearInputImage(field: string, fieldContentType: string, idInput: string) {
        this.dataUtils.clearInputImage(this.producteur, this.elementRef, field, fieldContentType, idInput);
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.producteur.id !== undefined) {
            this.subscribeToSaveResponse(this.producteurService.update(this.producteur));
        } else {
            this.subscribeToSaveResponse(this.producteurService.create(this.producteur));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IProducteur>>) {
        result.subscribe((res: HttpResponse<IProducteur>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackCommuneById(index: number, item: ICommune) {
        return item.id;
    }

    getSelected(selectedVals: Array<any>, option: any) {
        if (selectedVals) {
            for (let i = 0; i < selectedVals.length; i++) {
                if (option.id === selectedVals[i].id) {
                    return selectedVals[i];
                }
            }
        }
        return option;
    }
}
