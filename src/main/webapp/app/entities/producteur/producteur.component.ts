import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiAlertService, JhiDataUtils } from 'ng-jhipster';

import { IProducteur } from 'app/shared/model/producteur.model';
import { Principal } from 'app/core';
import { ProducteurService } from './producteur.service';

@Component({
    selector: 'jhi-producteur',
    templateUrl: './producteur.component.html'
})
export class ProducteurComponent implements OnInit, OnDestroy {
    producteurs: IProducteur[];
    currentAccount: any;
    eventSubscriber: Subscription;
    displayedColumns: string[] = ['id', 'email', 'photo', 'nom', 'prenom', 'password', 'description', 'commune', 'actions'];

    constructor(
        private producteurService: ProducteurService,
        private jhiAlertService: JhiAlertService,
        private dataUtils: JhiDataUtils,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {}

    loadAll() {
        this.producteurService.query().subscribe(
            (res: HttpResponse<IProducteur[]>) => {
                this.producteurs = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInProducteurs();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: IProducteur) {
        return item.id;
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }

    registerChangeInProducteurs() {
        this.eventSubscriber = this.eventManager.subscribe('producteurListModification', response => this.loadAll());
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
