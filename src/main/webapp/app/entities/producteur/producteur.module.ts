import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { BtbSharedModule } from 'app/shared';
import {
    ProducteurComponent,
    ProducteurDetailComponent,
    ProducteurUpdateComponent,
    ProducteurDeletePopupComponent,
    ProducteurDeleteDialogComponent,
    producteurRoute,
    producteurPopupRoute
} from './';

const ENTITY_STATES = [...producteurRoute, ...producteurPopupRoute];

@NgModule({
    imports: [BtbSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        ProducteurComponent,
        ProducteurDetailComponent,
        ProducteurUpdateComponent,
        ProducteurDeleteDialogComponent,
        ProducteurDeletePopupComponent
    ],
    entryComponents: [ProducteurComponent, ProducteurUpdateComponent, ProducteurDeleteDialogComponent, ProducteurDeletePopupComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class BtbProducteurModule {}
