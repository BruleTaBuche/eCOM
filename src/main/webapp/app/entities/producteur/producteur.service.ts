import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IProducteur } from 'app/shared/model/producteur.model';

type EntityResponseType = HttpResponse<IProducteur>;
type EntityArrayResponseType = HttpResponse<IProducteur[]>;

@Injectable({ providedIn: 'root' })
export class ProducteurService {
    private resourceUrl = SERVER_API_URL + 'api/producteurs';

    constructor(private http: HttpClient) {}

    create(producteur: IProducteur): Observable<EntityResponseType> {
        return this.http.post<IProducteur>(this.resourceUrl, producteur, { observe: 'response' });
    }

    update(producteur: IProducteur): Observable<EntityResponseType> {
        return this.http.put<IProducteur>(this.resourceUrl, producteur, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<IProducteur>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IProducteur[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
