import { Route } from '@angular/router';
import { ResultComponent } from '../btb/result/result.component';

export const HOME_ROUTE: Route = {
    path: '',
    component: ResultComponent,
    data: {
        authorities: [],
        pageTitle: 'home.title'
    }
};
