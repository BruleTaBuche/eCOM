import { IProducteur } from 'app/shared/model//producteur.model';
import { IOffre } from 'app/shared/model//offre.model';
import { IClient } from 'app/shared/model//client.model';

export interface ICommune {
    id?: number;
    nom?: string;
    codePostal?: number;
    producteurs?: IProducteur[];
    offres?: IOffre[];
    clients?: IClient[];
}

export class Commune implements ICommune {
    constructor(
        public id?: number,
        public nom?: string,
        public codePostal?: number,
        public producteurs?: IProducteur[],
        public offres?: IOffre[],
        public clients?: IClient[]
    ) {}
}
