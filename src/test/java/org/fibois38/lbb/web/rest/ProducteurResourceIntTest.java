package org.fibois38.lbb.web.rest;

import org.fibois38.lbb.BtbApp;

import org.fibois38.lbb.domain.Producteur;
import org.fibois38.lbb.repository.ProducteurRepository;
import org.fibois38.lbb.service.ProducteurService;
import org.fibois38.lbb.service.dto.ProducteurDTO;
import org.fibois38.lbb.service.mapper.ProducteurMapper;
import org.fibois38.lbb.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;


import static org.fibois38.lbb.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ProducteurResource REST controller.
 *
 * @see ProducteurResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = BtbApp.class)
public class ProducteurResourceIntTest {

    private static final String DEFAULT_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL = "BBBBBBBBBB";

    private static final byte[] DEFAULT_PHOTO = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_PHOTO = TestUtil.createByteArray(1, "1");
    private static final String DEFAULT_PHOTO_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_PHOTO_CONTENT_TYPE = "image/png";

    private static final String DEFAULT_NOM = "AAAAAAAAAA";
    private static final String UPDATED_NOM = "BBBBBBBBBB";

    private static final String DEFAULT_PRENOM = "AAAAAAAAAA";
    private static final String UPDATED_PRENOM = "BBBBBBBBBB";

    private static final String DEFAULT_PASSWORD = "AAAAAAAAAA";
    private static final String UPDATED_PASSWORD = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    @Autowired
    private ProducteurRepository producteurRepository;

    @Mock
    private ProducteurRepository producteurRepositoryMock;

    @Autowired
    private ProducteurMapper producteurMapper;
    

    @Mock
    private ProducteurService producteurServiceMock;

    @Autowired
    private ProducteurService producteurService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restProducteurMockMvc;

    private Producteur producteur;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ProducteurResource producteurResource = new ProducteurResource(producteurService);
        this.restProducteurMockMvc = MockMvcBuilders.standaloneSetup(producteurResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Producteur createEntity(EntityManager em) {
        Producteur producteur = new Producteur()
            .email(DEFAULT_EMAIL)
            .photo(DEFAULT_PHOTO)
            .photoContentType(DEFAULT_PHOTO_CONTENT_TYPE)
            .nom(DEFAULT_NOM)
            .prenom(DEFAULT_PRENOM)
            .password(DEFAULT_PASSWORD)
            .description(DEFAULT_DESCRIPTION);
        return producteur;
    }

    @Before
    public void initTest() {
        producteur = createEntity(em);
    }

    @Test
    @Transactional
    public void createProducteur() throws Exception {
        int databaseSizeBeforeCreate = producteurRepository.findAll().size();

        // Create the Producteur
        ProducteurDTO producteurDTO = producteurMapper.toDto(producteur);
        restProducteurMockMvc.perform(post("/api/producteurs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(producteurDTO)))
            .andExpect(status().isCreated());

        // Validate the Producteur in the database
        List<Producteur> producteurList = producteurRepository.findAll();
        assertThat(producteurList).hasSize(databaseSizeBeforeCreate + 1);
        Producteur testProducteur = producteurList.get(producteurList.size() - 1);
        assertThat(testProducteur.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testProducteur.getPhoto()).isEqualTo(DEFAULT_PHOTO);
        assertThat(testProducteur.getPhotoContentType()).isEqualTo(DEFAULT_PHOTO_CONTENT_TYPE);
        assertThat(testProducteur.getNom()).isEqualTo(DEFAULT_NOM);
        assertThat(testProducteur.getPrenom()).isEqualTo(DEFAULT_PRENOM);
        assertThat(testProducteur.getPassword()).isEqualTo(DEFAULT_PASSWORD);
        assertThat(testProducteur.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
    }

    @Test
    @Transactional
    public void createProducteurWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = producteurRepository.findAll().size();

        // Create the Producteur with an existing ID
        producteur.setId(1L);
        ProducteurDTO producteurDTO = producteurMapper.toDto(producteur);

        // An entity with an existing ID cannot be created, so this API call must fail
        restProducteurMockMvc.perform(post("/api/producteurs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(producteurDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Producteur in the database
        List<Producteur> producteurList = producteurRepository.findAll();
        assertThat(producteurList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkEmailIsRequired() throws Exception {
        int databaseSizeBeforeTest = producteurRepository.findAll().size();
        // set the field null
        producteur.setEmail(null);

        // Create the Producteur, which fails.
        ProducteurDTO producteurDTO = producteurMapper.toDto(producteur);

        restProducteurMockMvc.perform(post("/api/producteurs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(producteurDTO)))
            .andExpect(status().isBadRequest());

        List<Producteur> producteurList = producteurRepository.findAll();
        assertThat(producteurList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNomIsRequired() throws Exception {
        int databaseSizeBeforeTest = producteurRepository.findAll().size();
        // set the field null
        producteur.setNom(null);

        // Create the Producteur, which fails.
        ProducteurDTO producteurDTO = producteurMapper.toDto(producteur);

        restProducteurMockMvc.perform(post("/api/producteurs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(producteurDTO)))
            .andExpect(status().isBadRequest());

        List<Producteur> producteurList = producteurRepository.findAll();
        assertThat(producteurList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkPrenomIsRequired() throws Exception {
        int databaseSizeBeforeTest = producteurRepository.findAll().size();
        // set the field null
        producteur.setPrenom(null);

        // Create the Producteur, which fails.
        ProducteurDTO producteurDTO = producteurMapper.toDto(producteur);

        restProducteurMockMvc.perform(post("/api/producteurs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(producteurDTO)))
            .andExpect(status().isBadRequest());

        List<Producteur> producteurList = producteurRepository.findAll();
        assertThat(producteurList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllProducteurs() throws Exception {
        // Initialize the database
        producteurRepository.saveAndFlush(producteur);

        // Get all the producteurList
        restProducteurMockMvc.perform(get("/api/producteurs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(producteur.getId().intValue())))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL.toString())))
            .andExpect(jsonPath("$.[*].photoContentType").value(hasItem(DEFAULT_PHOTO_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].photo").value(hasItem(Base64Utils.encodeToString(DEFAULT_PHOTO))))
            .andExpect(jsonPath("$.[*].nom").value(hasItem(DEFAULT_NOM.toString())))
            .andExpect(jsonPath("$.[*].prenom").value(hasItem(DEFAULT_PRENOM.toString())))
            .andExpect(jsonPath("$.[*].password").value(hasItem(DEFAULT_PASSWORD.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())));
    }
    
    public void getAllProducteursWithEagerRelationshipsIsEnabled() throws Exception {
        ProducteurResource producteurResource = new ProducteurResource(producteurServiceMock);
        when(producteurServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        MockMvc restProducteurMockMvc = MockMvcBuilders.standaloneSetup(producteurResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();

        restProducteurMockMvc.perform(get("/api/producteurs?eagerload=true"))
        .andExpect(status().isOk());

        verify(producteurServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    public void getAllProducteursWithEagerRelationshipsIsNotEnabled() throws Exception {
        ProducteurResource producteurResource = new ProducteurResource(producteurServiceMock);
            when(producteurServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));
            MockMvc restProducteurMockMvc = MockMvcBuilders.standaloneSetup(producteurResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();

        restProducteurMockMvc.perform(get("/api/producteurs?eagerload=true"))
        .andExpect(status().isOk());

            verify(producteurServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @Test
    @Transactional
    public void getProducteur() throws Exception {
        // Initialize the database
        producteurRepository.saveAndFlush(producteur);

        // Get the producteur
        restProducteurMockMvc.perform(get("/api/producteurs/{id}", producteur.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(producteur.getId().intValue()))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL.toString()))
            .andExpect(jsonPath("$.photoContentType").value(DEFAULT_PHOTO_CONTENT_TYPE))
            .andExpect(jsonPath("$.photo").value(Base64Utils.encodeToString(DEFAULT_PHOTO)))
            .andExpect(jsonPath("$.nom").value(DEFAULT_NOM.toString()))
            .andExpect(jsonPath("$.prenom").value(DEFAULT_PRENOM.toString()))
            .andExpect(jsonPath("$.password").value(DEFAULT_PASSWORD.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingProducteur() throws Exception {
        // Get the producteur
        restProducteurMockMvc.perform(get("/api/producteurs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateProducteur() throws Exception {
        // Initialize the database
        producteurRepository.saveAndFlush(producteur);

        int databaseSizeBeforeUpdate = producteurRepository.findAll().size();

        // Update the producteur
        Producteur updatedProducteur = producteurRepository.findById(producteur.getId()).get();
        // Disconnect from session so that the updates on updatedProducteur are not directly saved in db
        em.detach(updatedProducteur);
        updatedProducteur
            .email(UPDATED_EMAIL)
            .photo(UPDATED_PHOTO)
            .photoContentType(UPDATED_PHOTO_CONTENT_TYPE)
            .nom(UPDATED_NOM)
            .prenom(UPDATED_PRENOM)
            .password(UPDATED_PASSWORD)
            .description(UPDATED_DESCRIPTION);
        ProducteurDTO producteurDTO = producteurMapper.toDto(updatedProducteur);

        restProducteurMockMvc.perform(put("/api/producteurs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(producteurDTO)))
            .andExpect(status().isOk());

        // Validate the Producteur in the database
        List<Producteur> producteurList = producteurRepository.findAll();
        assertThat(producteurList).hasSize(databaseSizeBeforeUpdate);
        Producteur testProducteur = producteurList.get(producteurList.size() - 1);
        assertThat(testProducteur.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testProducteur.getPhoto()).isEqualTo(UPDATED_PHOTO);
        assertThat(testProducteur.getPhotoContentType()).isEqualTo(UPDATED_PHOTO_CONTENT_TYPE);
        assertThat(testProducteur.getNom()).isEqualTo(UPDATED_NOM);
        assertThat(testProducteur.getPrenom()).isEqualTo(UPDATED_PRENOM);
        assertThat(testProducteur.getPassword()).isEqualTo(UPDATED_PASSWORD);
        assertThat(testProducteur.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void updateNonExistingProducteur() throws Exception {
        int databaseSizeBeforeUpdate = producteurRepository.findAll().size();

        // Create the Producteur
        ProducteurDTO producteurDTO = producteurMapper.toDto(producteur);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restProducteurMockMvc.perform(put("/api/producteurs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(producteurDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Producteur in the database
        List<Producteur> producteurList = producteurRepository.findAll();
        assertThat(producteurList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteProducteur() throws Exception {
        // Initialize the database
        producteurRepository.saveAndFlush(producteur);

        int databaseSizeBeforeDelete = producteurRepository.findAll().size();

        // Get the producteur
        restProducteurMockMvc.perform(delete("/api/producteurs/{id}", producteur.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Producteur> producteurList = producteurRepository.findAll();
        assertThat(producteurList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Producteur.class);
        Producteur producteur1 = new Producteur();
        producteur1.setId(1L);
        Producteur producteur2 = new Producteur();
        producteur2.setId(producteur1.getId());
        assertThat(producteur1).isEqualTo(producteur2);
        producteur2.setId(2L);
        assertThat(producteur1).isNotEqualTo(producteur2);
        producteur1.setId(null);
        assertThat(producteur1).isNotEqualTo(producteur2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ProducteurDTO.class);
        ProducteurDTO producteurDTO1 = new ProducteurDTO();
        producteurDTO1.setId(1L);
        ProducteurDTO producteurDTO2 = new ProducteurDTO();
        assertThat(producteurDTO1).isNotEqualTo(producteurDTO2);
        producteurDTO2.setId(producteurDTO1.getId());
        assertThat(producteurDTO1).isEqualTo(producteurDTO2);
        producteurDTO2.setId(2L);
        assertThat(producteurDTO1).isNotEqualTo(producteurDTO2);
        producteurDTO1.setId(null);
        assertThat(producteurDTO1).isNotEqualTo(producteurDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(producteurMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(producteurMapper.fromId(null)).isNull();
    }
}
