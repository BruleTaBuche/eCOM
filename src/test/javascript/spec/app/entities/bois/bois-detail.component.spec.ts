/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { BtbTestModule } from '../../../test.module';
import { BoisDetailComponent } from 'app/entities/bois/bois-detail.component';
import { Bois } from 'app/shared/model/bois.model';

describe('Component Tests', () => {
    describe('Bois Management Detail Component', () => {
        let comp: BoisDetailComponent;
        let fixture: ComponentFixture<BoisDetailComponent>;
        const route = ({ data: of({ bois: new Bois(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [BtbTestModule],
                declarations: [BoisDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(BoisDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(BoisDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.bois).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
