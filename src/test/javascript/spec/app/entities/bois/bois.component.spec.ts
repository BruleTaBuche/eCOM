/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { BtbTestModule } from '../../../test.module';
import { BoisComponent } from 'app/entities/bois/bois.component';
import { BoisService } from 'app/entities/bois/bois.service';
import { Bois } from 'app/shared/model/bois.model';

describe('Component Tests', () => {
    describe('Bois Management Component', () => {
        let comp: BoisComponent;
        let fixture: ComponentFixture<BoisComponent>;
        let service: BoisService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [BtbTestModule],
                declarations: [BoisComponent],
                providers: []
            })
                .overrideTemplate(BoisComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(BoisComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(BoisService);
        });

        it('Should call load all on init', () => {
            // GIVEN
            const headers = new HttpHeaders().append('link', 'link;link');
            spyOn(service, 'query').and.returnValue(
                of(
                    new HttpResponse({
                        body: [new Bois(123)],
                        headers
                    })
                )
            );

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.query).toHaveBeenCalled();
            expect(comp.bois[0]).toEqual(jasmine.objectContaining({ id: 123 }));
        });
    });
});
