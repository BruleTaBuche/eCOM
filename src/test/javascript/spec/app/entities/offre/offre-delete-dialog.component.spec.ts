/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { BtbTestModule } from '../../../test.module';
import { OffreDeleteDialogComponent } from 'app/entities/offre/offre-delete-dialog.component';
import { OffreService } from 'app/entities/offre/offre.service';

describe('Component Tests', () => {
    describe('Offre Management Delete Component', () => {
        let comp: OffreDeleteDialogComponent;
        let fixture: ComponentFixture<OffreDeleteDialogComponent>;
        let service: OffreService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [BtbTestModule],
                declarations: [OffreDeleteDialogComponent]
            })
                .overrideTemplate(OffreDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(OffreDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(OffreService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete', inject(
                [],
                fakeAsync(() => {
                    // GIVEN
                    spyOn(service, 'delete').and.returnValue(of({}));

                    // WHEN
                    comp.confirmDelete(123);
                    tick();

                    // THEN
                    expect(service.delete).toHaveBeenCalledWith(123);
                    expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                })
            ));
        });
    });
});
