/* tslint:disable max-line-length */
import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { of } from 'rxjs';
import { take, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { OffreService } from 'app/entities/offre/offre.service';
import { IOffre, Offre, Frequence } from 'app/shared/model/offre.model';

describe('Service Tests', () => {
    describe('Offre Service', () => {
        let injector: TestBed;
        let service: OffreService;
        let httpMock: HttpTestingController;
        let elemDefault: IOffre;
        let currentDate: moment.Moment;
        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [HttpClientTestingModule]
            });
            injector = getTestBed();
            service = injector.get(OffreService);
            httpMock = injector.get(HttpTestingController);
            currentDate = moment();

            elemDefault = new Offre(0, 'AAAAAAA', 0, 0, Frequence.PONCTUEL, 'AAAAAAA', 0, currentDate, currentDate);
        });

        describe('Service methods', async () => {
            it('should find an element', async () => {
                const returnedFromService = Object.assign(
                    {
                        dateLivraison: currentDate.format(DATE_FORMAT),
                        datePublication: currentDate.format(DATE_FORMAT)
                    },
                    elemDefault
                );
                service
                    .find(123)
                    .pipe(take(1))
                    .subscribe(resp => expect(resp).toMatchObject({ body: elemDefault }));

                const req = httpMock.expectOne({ method: 'GET' });
                req.flush(JSON.stringify(returnedFromService));
            });

            it('should create a Offre', async () => {
                const returnedFromService = Object.assign(
                    {
                        id: 0,
                        dateLivraison: currentDate.format(DATE_FORMAT),
                        datePublication: currentDate.format(DATE_FORMAT)
                    },
                    elemDefault
                );
                const expected = Object.assign(
                    {
                        dateLivraison: currentDate,
                        datePublication: currentDate
                    },
                    returnedFromService
                );
                service
                    .create(new Offre(null))
                    .pipe(take(1))
                    .subscribe(resp => expect(resp).toMatchObject({ body: expected }));
                const req = httpMock.expectOne({ method: 'POST' });
                req.flush(JSON.stringify(returnedFromService));
            });

            it('should update a Offre', async () => {
                const returnedFromService = Object.assign(
                    {
                        nom: 'BBBBBB',
                        prix: 1,
                        taille: 1,
                        frequence: 'BBBBBB',
                        description: 'BBBBBB',
                        stock: 1,
                        dateLivraison: currentDate.format(DATE_FORMAT),
                        datePublication: currentDate.format(DATE_FORMAT)
                    },
                    elemDefault
                );

                const expected = Object.assign(
                    {
                        dateLivraison: currentDate,
                        datePublication: currentDate
                    },
                    returnedFromService
                );
                service
                    .update(expected)
                    .pipe(take(1))
                    .subscribe(resp => expect(resp).toMatchObject({ body: expected }));
                const req = httpMock.expectOne({ method: 'PUT' });
                req.flush(JSON.stringify(returnedFromService));
            });

            it('should return a list of Offre', async () => {
                const returnedFromService = Object.assign(
                    {
                        nom: 'BBBBBB',
                        prix: 1,
                        taille: 1,
                        frequence: 'BBBBBB',
                        description: 'BBBBBB',
                        stock: 1,
                        dateLivraison: currentDate.format(DATE_FORMAT),
                        datePublication: currentDate.format(DATE_FORMAT)
                    },
                    elemDefault
                );
                const expected = Object.assign(
                    {
                        dateLivraison: currentDate,
                        datePublication: currentDate
                    },
                    returnedFromService
                );
                service
                    .query(expected)
                    .pipe(
                        take(1),
                        map(resp => resp.body)
                    )
                    .subscribe(body => expect(body).toContainEqual(expected));
                const req = httpMock.expectOne({ method: 'GET' });
                req.flush(JSON.stringify([returnedFromService]));
                httpMock.verify();
            });

            it('should delete a Offre', async () => {
                const rxPromise = service.delete(123).subscribe(resp => expect(resp.ok));

                const req = httpMock.expectOne({ method: 'DELETE' });
                req.flush({ status: 200 });
            });
        });

        afterEach(() => {
            httpMock.verify();
        });
    });
});
