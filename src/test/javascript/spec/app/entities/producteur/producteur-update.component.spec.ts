/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { BtbTestModule } from '../../../test.module';
import { ProducteurUpdateComponent } from 'app/entities/producteur/producteur-update.component';
import { ProducteurService } from 'app/entities/producteur/producteur.service';
import { Producteur } from 'app/shared/model/producteur.model';

describe('Component Tests', () => {
    describe('Producteur Management Update Component', () => {
        let comp: ProducteurUpdateComponent;
        let fixture: ComponentFixture<ProducteurUpdateComponent>;
        let service: ProducteurService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [BtbTestModule],
                declarations: [ProducteurUpdateComponent]
            })
                .overrideTemplate(ProducteurUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(ProducteurUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ProducteurService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new Producteur(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.producteur = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new Producteur();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.producteur = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
