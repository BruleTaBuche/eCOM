/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { BtbTestModule } from '../../../test.module';
import { ProducteurComponent } from 'app/entities/producteur/producteur.component';
import { ProducteurService } from 'app/entities/producteur/producteur.service';
import { Producteur } from 'app/shared/model/producteur.model';

describe('Component Tests', () => {
    describe('Producteur Management Component', () => {
        let comp: ProducteurComponent;
        let fixture: ComponentFixture<ProducteurComponent>;
        let service: ProducteurService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [BtbTestModule],
                declarations: [ProducteurComponent],
                providers: []
            })
                .overrideTemplate(ProducteurComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(ProducteurComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ProducteurService);
        });

        it('Should call load all on init', () => {
            // GIVEN
            const headers = new HttpHeaders().append('link', 'link;link');
            spyOn(service, 'query').and.returnValue(
                of(
                    new HttpResponse({
                        body: [new Producteur(123)],
                        headers
                    })
                )
            );

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.query).toHaveBeenCalled();
            expect(comp.producteurs[0]).toEqual(jasmine.objectContaining({ id: 123 }));
        });
    });
});
